print(10*'#%')
print('LADOWARKA PACZEK')
print(10*'#%')

MAX_WEIGHT_OF_PACKAGE = 20
max_parecel_in_package = 0
package_to_send = 0
package = []
package_weight = []

while True:

    parcel_weight = input('Podaj wage paczki od 1kg do 10kg(tylko calkowite liczby): ')

    if not (parcel_weight.isnumeric()):
        print('podales litere! sprobuj jeszcze raz podajac liczbe calkowita.')
        continue
    
    else:
        parcel_weight = int(parcel_weight)

    if parcel_weight == 0:
        print('KONIEC ZALADUNKU')
        print(f'Liczba wyslanych paczek wynosi: {package_to_send}')
        print(f'Suma wyslanych kilogramow: {sum(package_weight)} kg')
        sum_empty_kg = package_to_send * 20 - sum(package_weight)
        print(f'Suma "pustych" kilogramow: {sum_empty_kg} kg')
        break

    if float(parcel_weight) < 1.0 :
        print('Podales za mala liczbe!, Sprobuj jeszcze raz.')
        continue

    elif parcel_weight > 10 :
        print('Podales za duza liczbe!, Sprobuj jeszcze raz.')
        continue

    elif sum(package) + parcel_weight >= MAX_WEIGHT_OF_PACKAGE:
        package_to_send += 1
        print(f'Paczkazostala wyslana. Wyslanych paczek jest: {package_to_send}')
        package_weight.append(sum(package))
        package.clear()
        package.append(parcel_weight)
        continue

    else:
        package.append(parcel_weight)
        print(f'paczka wazy: {sum(package)} kg')
        continue